# README #

A utility for selective check-outs from large SVN repositories.
Assumes a local installation of Python 2.7 and Subversion 1.5.

## Usage ##

```
#!text
Usage: svnselect.py -u <repo> [-l|-r] [-p <pattern>] [-c]
       svnselect.py -s <cfg.py>

-h, --help	Displays this screen.
-l, --ls	Non-recursive listing of files and directories.
-r, --rls	Recursive listing of files and directories.
-p, --pat	Filters the listing with a regular expression.
-c, --co	Performs an SVN check-out / update.
-s, --scr	Runs a python script.

Note that the operations are performed in the order that the 
command-line arguments are provided.
```

## Examples ##
A few examples on how to use it from the command-line.

### Recursive check-out of *.ods ###
```
#!bash

svnselect.py -u https://your_repo/docs/ -r -p "^(.*).ods" -c
```
Here's what it does:
1. Performs a recursive listing of files and directories in the SVN repository https://your_repo/docs/.

2. Applies a regular expression pattern that matches only OpenOffice spreadsheet files.

3. Performs an SVN check-out on the spreadsheet files.

### Check-out of *.log files ###
Let's say there's a SVN repository with a lot of directories that start with year and date.
Inside each directory, there are log files scattered across the subdirectories. The whole repository
is several GiB and we only need the log files from June 4th, 2014.
```
#!bash

svnselect.py -u https://your_repo/logs/ -l -p "2014.06.04.*" -r -p ".*.log" -c
```

Here's what it does:
1. Performs a non-recursive listing of files and directories in the SVN repository https://your_repo/logs/.

2. Applies a regular expression pattern that matches directories starting with June 4th, 2014.

3. Performs a recursive listing inside the matching directories.

4. Applies a regular expression pattern that matches log files.

5. Performs an SVN check-out on the log files.

### Python script ###
A python script version for the "*Check-out of log files*" example.
```
#!bash

svnselect.py -s cfg.py
```
The contents of *cfg.py*:
```
#!python

"""
Two-stage regular expressions to find directories and check out *.log files
from the directories.
"""
def test2():
        svns = SVNSelect("https://your_repo/logs/")
        # List repository root directory non-recursively
        dirs = svns.svn_list("", False)
        # Select directories from June 4th, 2014
        dirs = svns.svn_select(dirs, r"2014.06.04.*")
        # Recursively list files in the selected directories
        files = svns.svn_list(dirs, True)
        # Select all *.log files
        files = svns.svn_select(files, r".*.log")
        # Perform an SVN checkout on the files
        svns.svn_co(files)

test2()
```


## License ##
SVNSelect is distributed under the [MIT license][1].

[1]: http://opensource.org/licenses/mit-license.php