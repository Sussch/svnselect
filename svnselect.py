#!/usr/bin/python
"""
 A script to selectively check out a subset of files or directories
 from a large SVN repository.

 Indrek Synter 2014

 The contents of this file are subject to the MIT License.
 The license is available at http://opensource.org/licenses/MIT
"""

import re
import os, sys
import getopt
import urlparse
import subprocess

"""
A class to selectively check out files or directories
from an SVN repository.
"""
class SVNSelect:
	"""
	Initialize SVNSelect on an SVN repository.
	"""
	def __init__(self, base_address):
		self.base_address = base_address

	"""
	List SVN repository contents recursively.
	This is pretty slow.
	"""
	def svn_list(self, paths, recursive):
		entries = []
		# Ensure that the input is a list of paths
		if not isinstance(paths, list):
			paths = [paths]
		# List the contents of all paths
		for path in paths:
			if path != '':
				print "Listing {}".format(path)
			else:
				print "Listing ."
			# Calculate an address from the path
			address = urlparse.urljoin(self.base_address, path)
			# SVN list on each address
			if not recursive:
				listing = subprocess.check_output(["svn", "ls", address]).split('\n')
			else:
				listing = subprocess.check_output(["svn", "ls", address, "-R"]).split('\n')
			entries.extend([os.path.join(path, x) for x in listing])
		return entries

	"""
	Check if the path is a directory name.
	"""
	def is_dir(self, fname):
		return(len(fname) >= 1 and fname[-1] == '/')

	"""
	Check if the path matches the regular expression.
	"""
	def regex_match(self, pattern, fname):
		return re.match(pattern, fname)

	"""
	Selects SVN directories and files, which match
	the regular expression pattern.
	"""
	def svn_select(self, entries, pattern):
		i = 0
		matches = []
		while i < len(entries):
			entry = entries[i]
			# Matches the regular expression?
			if self.regex_match(pattern, entry):
				matches.append(entry)
			i += 1
		return matches

	"""
	Check out a list of files and/or directories.
	"""
	def svn_co(self, paths):
		for path in paths:
			old_cwd = os.getcwd()

			address = urlparse.urljoin(self.base_address, path)
			dir_address = os.path.dirname(address)
			dir_name = os.path.dirname(path)
			file_name = os.path.basename(path)

			# Directory does not exist yet?
			# Check out an empty directory via SVN.
			if dir_name != '' and not os.path.exists(dir_name):
				print subprocess.check_output(["svn", "co", dir_address, dir_name, "--depth", "empty"])
			# Enter the directory
			if dir_name != '':
				print "cd {}".format(dir_name)
				os.chdir(dir_name)
			# SVN update the file there
			print subprocess.check_output(["svn", "up", file_name])
			# Leave the directory
			print "cd {}".format(old_cwd)
			os.chdir(old_cwd)

"""
Recursively check out all *.ods files from the repository.
"""
def test1():
	svns = SVNSelect("https://your_repo/docs/")
	# List repository root directory recursively
	dirs = svns.svn_list("", True)
	# Select all *.ods files
	files = svns.svn_select(dirs, r"^(.*).ods")
	# Perform an SVN checkout on the files
	svns.svn_co(files)

"""
Two-stage regular expressions to find directories and check out *.log files
from the directories.
"""
def test2():
	svns = SVNSelect("https://your_repo/logs/")
	# List repository root directory non-recursively
	dirs = svns.svn_list("", False)
	# Select directories from June 4th, 2014
	dirs = svns.svn_select(dirs, r"2014.06.04.*")
	# Recursively list files in the selected directories
	files = svns.svn_list(dirs, True)
	# Select all *.log files
	files = svns.svn_select(files, r".*.log")
	# Perform an SVN checkout on the files
	svns.svn_co(files)

def run_cfg(path):
	execfile(path, globals())

def help():
	print "Usage: svnselect.py -u <repo> [-l|-r] [-p <pattern>] [-c]"
	print "       svnselect.py -s <cfg.py>"
	print
	print "-h, --help\tDisplays this screen."
	print "-l, --ls\tNon-recursive listing of files and directories."
	print "-r, --rls\tRecursive listing of files and directories."
	print "-p, --pat\tFilters the listing with a regular expression."
	print "-c, --co\tPerforms an SVN check-out / update."
	print "-s, --scr\tRuns a python script."
	print
	print "Note that the operations are performed in the order that the \ncommand-line arguments are provided."

def check_svns(svns):
	if svns == None:
		print "Repository URL parameter is needed."
		sys.exit(2)

def main(argv):
	svns = None
	paths = ""
	pout = True

	if len(argv) < 2:
		help()
		sys.exit(1)

	for i in range(1, len(argv)):
		# Get command-line option and argument (if any)
		opt = argv[i]
		if i < len(argv) - 1:
			arg = argv[i + 1]
		else:
			arg = ""

		# Help?
		if opt in ("-h", "--help"):
			help()
			sys.exit()
		# A configuration script?
		elif opt in ("-s", "--scr"):
			run_cfg(arg)
			sys.exit()
		# An SVN repository URL?
		elif opt in ("-u", "--url"):
			svns = SVNSelect(arg)
			print "URL: " + arg
		# A non-recursive listing?
		elif opt in ("-l", "--ls"):
			check_svns(svns)
			paths = svns.svn_list(paths, False)
		# A recursive listing?
		elif opt in ("-r", "--rls"):
			check_svns(svns)
			paths = svns.svn_list(paths, True)
		# A selection pattern?
		elif opt in ("-p", "--pat"):
			check_svns(svns)
			paths = svns.svn_select(paths, arg)
		# An SVN check-out / update?
		elif opt in ("-c", "--co"):
			check_svns(svns)
			svns.svn_co(paths)
			pout = False
	# Print the output
	if pout:
		print
		print "\n".join(paths)

if __name__ == '__main__':
	main(sys.argv)
